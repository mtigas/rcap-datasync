#!/usr/bin/env python
# This file is part of rcap-datasync by Mike Tigas
#   https://0xacab.org/mtigas/rcap-datasync
# Copyright © 2020 Mike Tigas
#   https://mike.tig.as/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
#  at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import csv
from pprint import pprint
from decimal import Decimal as D

import argparse
import sys


rc_non_numeric_fields = frozenset({
    "Trap name",
})
def is_rc_field_not_numeric(fieldname):
    return (fieldname in rc_non_numeric_fields)


ap_skip_hed_w_substring = [
    "AP Info:[",
]
def is_ap_field_ok(fieldname):
    for ss in ap_skip_hed_w_substring:
        if ss in fieldname:
            return False
    return True

#####

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--offset', default="0.0", type=str)


    parser.add_argument('--rc-session', required=False, type=int)

    parser.add_argument('rc_file', type=argparse.FileType('r', encoding='utf-8'),
                        help='RaceChrono VBO file')
    parser.add_argument('ap_file', type=argparse.FileType('r', encoding='ascii'),
                        help='Cobb AccessPort datalogger CSV')


    args = parser.parse_args()
    # print(args.accumulate(args.integers))


    ########################################
    ap_data = []
    
    # the last header col in the access port export is a string that looks like this:
    # "AP Info:[AP3-VLK-003 v1.7.4.0-18570][USDM Volkswagen CCF Mk7 Golf R...][Reflash: EQT - Stage 1 93 v2.51 - TC.ptm - TCM: EQT - DSG - IS38 v2.03 NASKD - 12.ptm]"
    ap_info_str = ""
    
    # we want our output headers in a particular order, so start keeping track here
    ap_headers = []
    
    ap_reader = csv.reader(args.ap_file, delimiter=',')
    ap_hed_row = next(ap_reader)
    for fieldname in ap_hed_row:
        if "AP Info:[" in fieldname:
            # store the info str, but don't keep it as a header
            ap_info_str = fieldname.strip()
        elif (fieldname not in ap_headers) and (is_ap_field_ok(fieldname)):
            ap_headers.append(fieldname)
    
    args.ap_file.seek(0)
    ap_reader = csv.DictReader(args.ap_file)
    for row in ap_reader:
        # ap_data.append(row)
        row_data = {}
        for fieldname,val in row.items():
            if is_ap_field_ok(fieldname):
                row_data[fieldname] = D(val)
        ap_data.append(row_data)
    
    
    # for hed,val in ap_data[0].items():
    #     print(f"{hed} - {val}")
    
    ########################################
    rc_data = []

    rc_hed_row = None
    rcdata_seek_chrs = 0
    rc_prev_line = ""
    rc_line_no = 0
    
    # seek until we get to the real CSV header. (it's separated from a text header
    # by an empty line.)
    for line in args.rc_file:
        rc_line_no += 1
        rcdata_seek_chrs += len(line) + 1
        if rc_line_no > 2 and rc_prev_line.strip() == "":
            rc_hed_row = line.strip()
        rc_prev_line = line
        if rc_hed_row:
            break
    
    #### debug -- seek to just after the header line & print; make sure we're
    #### seeking to the csv
    # args.rc_file.seek(rcdata_seek_chrs)
    # print(args.rc_file.readline().strip())
    
    rc_headers = []
    for fieldname in rc_hed_row.split(","):
        if fieldname not in rc_headers:
            rc_headers.append(fieldname)
        else:
            # the csv data might have columns with the same name; make unique
            for i in range(1,10000):
                indexed_fieldname = f"{fieldname} {i}"
                if indexed_fieldname not in rc_headers:
                    rc_headers.append(indexed_fieldname)
                    break

    # pprint(rc_hed_row)
    # pprint(rc_headers)
    # sys.exit(0)
    
    rc_time_zero = D(0)
    # first_row = next(rc_reader)
    # pprint(first_row)
    # r = {}
    # for k,v in first_row.items():
    #     if k == "Time (s)":
    #         k = "Time (sec)"
    #     if is_rc_field_not_numeric(k):
    #         r[k] = v
    #     else:
    #         if v == '':
    #             r[k] = None
    #         else:
    #             r[k] = D(v)
    # rc_time_zero = D(r['Time (sec)'])
    # 
    # pprint(r)
    # 
    # sys.exit(0)
    # 
    # 
    # print(f"Native Offset (RC time zero): {rc_time_zero}")
    # print(f"Additional Offset (--offset flag): {D(args.offset)}")
    offset = D(args.offset) + rc_time_zero
    # print(f"Final Offset: {offset}")

    #####

    
    args.rc_file.seek(rcdata_seek_chrs)
    print(args.rc_file.readline().strip())
    args.rc_file.seek(rcdata_seek_chrs)
    rc_reader = csv.DictReader(args.rc_file, fieldnames=rc_headers)
    
    did_have_session = False

    print()
    for row in rc_reader:
        r = {}
        for k,v in row.items():
            if k == "Time (s)":
                k = "Time (sec)"
            if is_rc_field_not_numeric(k):
                r[k] = v
            else:
                if v == '':
                    r[k] = None
                else:
                    r[k] = D(v)
                    
        if args.rc_session != None:
            if not did_have_session:
                if args.rc_session == r['Session fragment #']:
                    did_have_session = True
                else:
                    continue
            elif did_have_session and (args.rc_session != r['Session fragment #']):
                # got data for session we wanted, and we are no longer in that
                # session in the csv data
                break

        #r['Vehicle Speed (mph)'] = "{:.2f}".format(r['velocity'] / D('1.609344'))
        if r['Speed (m/s)']:
            r['Vehicle Speed (mph)'] = "{:.2f}".format(r['Speed (m/s)'] / D('0.44704'))
        else:
            r['Vehicle Speed (mph)'] = None

        if r['Time (sec)'] >= D(0):
            rc_data.append(r)

    # for hed,val in rc_data[0].items():
    #     print(f"{hed} - {val}")
            
    # for col,val in enumerate(rc_data[0]):
    #     hed = headers_2[col]
    #     print(f"{hed} - {val}")
    
        
    #full_data = []

    ap_out_name = f"{args.ap_file.name[:-4]}-synced.csv"
    # print(ap_out_name)
    if args.rc_session != None:
        rc_out_name = f"{args.rc_file.name[:-4]}-synced_{args.rc_session}.csv"
    else:
        rc_out_name = f"{args.rc_file.name[:-4]}-synced.csv"

    # print(rc_out_name)
    
    with open(ap_out_name, 'w') as ap_out:
        ap_writer = csv.DictWriter(ap_out, ap_headers)
        ap_writer.writeheader()
    
        ap_peak_speed = D(0)
        ap_peak_time = D(0)

        for entry in ap_data:
            output_entry = {}
            for header in ap_headers:
                if header in entry.keys():
                    output_entry[header] = entry[header]
                else:
                    output_entry[header] = None
            if output_entry['Vehicle Speed (mph)'] > ap_peak_speed:
                ap_peak_speed = output_entry['Vehicle Speed (mph)']
                ap_peak_time = output_entry['Time (sec)']
            # full_data.append(output_entry)
            ap_writer.writerow(output_entry)

    rc_out_headers = rc_headers
    if "Time (sec)" not in rc_out_headers:
        rc_out_headers.insert(0, "Time (sec)")
    if "Vehicle Speed (mph)" not in rc_out_headers:
        rc_out_headers.insert(1, "Vehicle Speed (mph)")

    with open(rc_out_name, 'w') as rc_out:
        rc_writer = csv.DictWriter(rc_out, rc_out_headers)
        rc_writer.writeheader()
        
        rc_peak_speed = D(0)
        rc_peak_time = D(0)

        for entry in rc_data:
            output_entry = {}
            for header in rc_headers:
                if header in entry.keys():
                    output_entry[header] = entry[header]
                else:
                    output_entry[header] = None
            if (output_entry['Vehicle Speed (mph)']) and (D(output_entry['Vehicle Speed (mph)']) > rc_peak_speed):
                rc_peak_speed = D(output_entry['Vehicle Speed (mph)'])
                rc_peak_time = output_entry['Time (sec)']
            # pprint(output_entry)
            # full_data.append(output_entry)
            rc_writer.writerow(output_entry)
            
    pprint(ap_peak_speed)
    pprint(ap_peak_time)
    pprint(rc_peak_speed)
    pprint(rc_peak_time)

    ##### OLD code for writing full combined file
    # boost = None
    # rpm = None
    # accel = None
    # v_speed = None
    # g_speed = None
    # for row in sorted(full_data, key=lambda row: row["Time (sec)"]):
    #     writer.writerow(row)
    #     if row.get('Relative Manifold Pressure (psi)', None) is not None:
    #         boost = row['Relative Manifold Pressure (psi)']
    #         p_boost = f"{boost}"
    #     elif row.get('Boost Press. (psi)', None) is not None:
    #         boost = row['Boost Press. (psi)']
    #         p_boost = f"{boost}"
    #     else:
    #         p_boost = f"({boost})"
    # 
    #     if row.get('Engine Speed (RPM)', None) is not None:
    #         rpm = row['Engine Speed (RPM)']
    #         p_rpm = f"{rpm}"
    #     else:
    #         p_rpm = f"({rpm})"
    # 
    #     if row.get('Accel Pedal Position (%)', None) is not None:
    #         accel = row['Accel Pedal Position (%)']
    #         p_accel = f"{accel}"
    #     else:
    #         p_accel = f"({accel})"
    # 
    #     if row.get('Vehicle Speed (mph)', None) is not None:
    #         v_speed = row['Vehicle Speed (mph)']
    #         p_v_speed = f"{v_speed}"
    #     else:
    #         p_v_speed = f"({v_speed})"
    # 
    #     # print(f"{row['Time (sec)']} - boost {p_boost} psi - {p_rpm} RPM - accel {p_accel}% - speed {p_v_speed} mph")
