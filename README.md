# RaceChrono/AccessPort data sync tool

&copy; 2020, [Mike Tigas](https://mike.tig.as/); [GNU AGPLv3 License](LICENSE.txt)

---

## Managing Data Exports

AccessPort: these just come in one flavor, so nothing to worry about there

RaceChrono: from a session, go to Export; then select "RaceRender/DashWare (.csv)" as the application, and keep "Whole session" as the scope. Under "show more", you can either keep "Original" as the Update rate or manually select a slower (lower-Hz) rate.